﻿using AFTest.Model;
using MyToolkit.Model;

namespace AFTest.ViewModel {
	public class CheckItemViewModel:ObservableObject {
		readonly CheckViewModel _owner;
		readonly CheckItem _object;

		public CheckItemViewModel(CheckViewModel owner, CheckItem @object) {
			_owner = owner;
			_object = @object;
		}

		public Product Product => _object.Product;

		public double Count {
			get => _object.Count;
			set {
				if (value > Product.Quantity) value = Product.Quantity;
				if (value == _object.Count) return;
				_object.Count = value;
				RaisePropertyChanged(nameof(Count));

				Amount = Count * Product.Price;
			}
		}

		public double Amount {
			get => _object.Amount;
			set {
				if (value == _object.Amount) return;
				_object.Amount = value;
				RaisePropertyChanged(nameof(Amount));

				_owner.RecalcSum();
			}
		}

		internal void Delete(Check check) {
			check.Items.Remove(_object);
			_object.Check = null;
		}
	}
}