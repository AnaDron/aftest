﻿using System;
using System.Windows.Threading;
using AFTest.Model;
using MyToolkit.Model;

namespace AFTest.ViewModel {
	public class MainWindowModel:ObservableObject {
		readonly IProductRepository _productRepository;
		readonly ICheckService _checkService;

		object _context;
		Check _check;

		public MainWindowModel(IProductRepository productRepository, ICheckService checkService) {
			_productRepository = productRepository;
			_checkService = checkService;

			OpenCheck();

			var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1) };
			timer.Tick += (s, a) => RaisePropertyChanged(nameof(DateTime));
			timer.Start();
		}

		public object Context {
			get => _context;
			internal set => Set(ref _context, value);
		}

		public DateTime DateTime => DateTime.Now;

		void OpenCheck() {
			_check = _checkService.Open();
			ReopenCheck();
		}

		void ReopenCheck() {
			Context = new OpenCheckViewModel(_check, _productRepository, ClosingCheck);
		}

		void ClosingCheck() {
			Context = new CloseCheckViewModel(_check, CloseCheck, ReopenCheck);
		}

		void CloseCheck() {
			_checkService.Close(_check);
			OpenCheck();
		}
	}
}