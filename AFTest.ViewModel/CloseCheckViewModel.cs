﻿using System;
using System.Windows.Input;
using AFTest.Model;
using MyToolkit.Command;
using MyToolkit.Model;

namespace AFTest.ViewModel {
	public class CloseCheckViewModel:ObservableObject {
		readonly RelayCommand _closeCommand;
		readonly RelayCommand _cancelCommand;
		double _change;
		double _money;

		public CloseCheckViewModel(Check check, Action close, Action cancel) {
			Check = check;
			_closeCommand = new RelayCommand(close, () => Money >= Check.Sum);
			_cancelCommand = new RelayCommand(cancel);
			_change = -Check.Sum;
		}

		public Check Check { get; }

		public ICommand CloseCommand => _closeCommand;
		public ICommand CancelCommand => _cancelCommand;

		public double Money {
			get => _money;
			set {
				if (value == _money) return;
				_money = value;
				Change = value - Check.Sum;
			}
		}

		public double Change {
			get => _change;
			private set => Set(ref _change, value);
		}
	}
}