﻿using System;
using System.Linq;
using System.Windows.Input;
using AFTest.Model;
using MyToolkit.Command;
using MyToolkit.Model;

namespace AFTest.ViewModel {
	public class OpenCheckViewModel:ObservableObject {
		readonly IProductRepository _productRepository;

		readonly RelayCommand _clearCommand;
		readonly RelayCommand _prevHintCommand;
		readonly RelayCommand _nextHintCommand;
		readonly RelayCommand _enterCommand;
		readonly RelayCommand _deleteCommand;
		readonly RelayCommand _closeCommand;

		string _text;
		CheckItemViewModel _selectedItem;

		abstract class Behavior {
			protected readonly OpenCheckViewModel Owner;

			protected Behavior(OpenCheckViewModel owner) {
				Owner = owner;
			}

			public virtual void Dispose() { }

			protected string Text {
				get => Owner.Text;
				set => Owner.Text = value;
			}

			protected CheckViewModel Check => Owner.Check;

			protected CheckItemViewModel SelectedItem {
				get => Owner.SelectedItem;
				set => Owner.SelectedItem = value;
			}

			public abstract void Prev();
			public abstract void Next();
			public abstract void Apply();
		}

		class AddBehavior:Behavior {
			static int Decrease(int index, int maxIndex) => index == 0 ? maxIndex : index - 1;
			static int Increase(int index, int maxIndex) => index == maxIndex ? 0 : index + 1;

			Product[] _availableProducts;
			int _hintIndex = -1;
			string _hint;

			public AddBehavior(OpenCheckViewModel owner) : base(owner) { }

			public override void Dispose() {
				Hint = null;
			}

			public string Hint {
				get => _hint;
				private set {
					if (value == _hint) return;
					_hint = value;
					Owner.RaisePropertyChanged(nameof(OpenCheckViewModel.Hint));
				}
			}

			void HintUpdate() {
				var hint = Text + _availableProducts[_hintIndex].Name.Substring(Text.Length);
				if (_availableProducts.Length > 1) hint += $" (+{_availableProducts.Length - 1})";
				Hint = hint;
			}

			public override void Prev() {
				_hintIndex = Decrease(_hintIndex, _availableProducts.Length - 1);
				HintUpdate();
			}

			public override void Next() {
				_hintIndex = Increase(_hintIndex, _availableProducts.Length - 1);
				HintUpdate();
			}

			public override void Apply() {
				var product = _availableProducts.First(x => _hint.StartsWith(x.Name, StringComparison.OrdinalIgnoreCase));
				var checkItemVM = Check.Items.FirstOrDefault(x => x.Product == product);
				if (checkItemVM == null) {
					var checkItem = new CheckItem { Product = product };
					checkItemVM = Check.Add(checkItem);
				}
				checkItemVM.Count++;
				SelectedItem = checkItemVM;
			}

			public void Update(Product[] availableProducts) {
				_availableProducts = availableProducts;

				if (_hint == null)
					_hintIndex = 0;
				else {
					_hintIndex = _availableProducts.IndexOf(x => _hint.StartsWith(x.Name, StringComparison.OrdinalIgnoreCase));
					if (_hintIndex == -1) _hintIndex = 0;
				}

				HintUpdate();
			}
		}

		class EditBehavior:Behavior {
			public EditBehavior(OpenCheckViewModel owner) : base(owner) { }

			void Move(int step) {
				var index = Check.Items.IndexOf(SelectedItem) + step;
				if (index < 0 || index >= Check.Items.Count) return;
				SelectedItem = Check.Items[index];
			}

			public override void Prev() {
				Move(-1);
			}

			public override void Next() {
				Move(1);
			}

			public override void Apply() {
				SelectedItem.Count = double.Parse(Text);
				Text = SelectedItem.Count.ToString("F3");
			}
		}

		Behavior _behavior;

		internal OpenCheckViewModel(Check check, IProductRepository productRepository, Action close) {
			_productRepository = productRepository;

			_clearCommand = new RelayCommand(() => {
				Check.Clear();
			});

			_prevHintCommand = new RelayCommand(() => { _behavior.Prev(); }, () => _behavior != null);
			_nextHintCommand = new RelayCommand(() => { _behavior.Next(); }, () => _behavior != null);
			_enterCommand = new RelayCommand(() => { _behavior.Apply(); }, () => _behavior != null);

			_deleteCommand = new RelayCommand(() => {
				var index = Check.Items.IndexOf(_selectedItem);
				Check.Delete(_selectedItem);
				if (Check.Items.Count == 0) {
					SelectedItem = null;
					return;
				}
				if (index == Check.Items.Count) index--;
				SelectedItem = Check.Items[index];
			}, () => _selectedItem != null);

			_closeCommand = new RelayCommand(close, () => Check.Items.Count != 0);

			Check = new CheckViewModel(check);

			if (Check.Items.Count > 0) SelectedItem = Check.Items[Check.Items.Count - 1];
		}

		Behavior BehaviorSet {
			set {
				if (value == _behavior) return;
				_behavior?.Dispose();
				_behavior = value;
			}
		}

		public ICommand ClearCommand => _clearCommand;
		public ICommand PrevHintCommand => _prevHintCommand;
		public ICommand NextHintCommand => _nextHintCommand;
		public ICommand EnterCommand => _enterCommand;
		public ICommand DeleteCommand => _deleteCommand;
		public ICommand CloseCommand => _closeCommand;

		public CheckViewModel Check { get; }

		public CheckItemViewModel SelectedItem {
			get => _selectedItem;
			set {
				if (Set(ref _selectedItem, value))
					Text = _selectedItem?.Count.ToString("F3");
			}
		}

		public string Text {
			get => _text;
			set {
				if (value == _text) return;
				_text = value;
				if (string.IsNullOrEmpty(value))
					BehaviorSet = null;
				else if (Check.Items.Count > 0 && double.TryParse(_text, out var count) && count > 0)
					BehaviorSet = new EditBehavior(this);
				else {
					var availableProducts = _productRepository.GetProductsByNameStartsWith(value).Take(10).ToArray();
					if (availableProducts.Length == 0)
						BehaviorSet = null;
					else {
						if (!(_behavior is AddBehavior)) BehaviorSet = new AddBehavior(this);
						((AddBehavior)_behavior).Update(availableProducts);
					}
				}
				RaisePropertyChanged(nameof(Text));
			}
		}

		public string Hint => (_behavior as AddBehavior)?.Hint;
	}
}