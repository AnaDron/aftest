﻿using System.Collections.ObjectModel;
using System.Linq;
using AFTest.Model;
using MyToolkit.Model;

namespace AFTest.ViewModel {
	public class CheckViewModel:ObservableObject {
		readonly Check _object;

		public CheckViewModel(Check @object) {
			_object = @object;
			Items = new ObservableCollection<CheckItemViewModel>(@object.Items.Select(x => new CheckItemViewModel(this, x)));
		}

		public int Id => _object.CheckId;

		public ObservableCollection<CheckItemViewModel> Items { get; }

		public double Sum {
			get => _object.Sum;
			set {
				if (value == _object.Sum) return;
				_object.Sum = value;
				RaisePropertyChanged(nameof(Sum));
			}
		}

		internal void Clear() {
			foreach (var item in Items) item.Delete(_object);
			Items.Clear();
			RecalcSum();
		}

		internal CheckItemViewModel Add(CheckItem item) {
			item.Check = _object;
			_object.Items.Add(item);
			var itemVM = new CheckItemViewModel(this, item);
			Items.Add(itemVM);
			RecalcSum();
			return itemVM;
		}

		internal void Delete(CheckItemViewModel itemVM) {
			itemVM.Delete(_object);
			Items.Remove(itemVM);
			RecalcSum();
		}

		internal void RecalcSum() {
			Sum = _object.Items.Sum(x => x.Amount);
		}
	}
}