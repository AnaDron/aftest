﻿using System;
using System.Collections.Generic;

namespace AFTest.ViewModel {
	static class Extensions {
		public static int IndexOf<T>(this IEnumerable<T> self, Predicate<T> predicate) {
			var i = 0;

			foreach (var item in self) {
				if (predicate(item)) return i;
				i++;
			}

			return -1;
		} 
	}
}