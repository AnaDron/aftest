﻿using System.Collections.Generic;

namespace AFTest.Model {
	public interface IProductRepository {
		IEnumerable<Product> GetProductsByNameStartsWith(string nameStart);
	}
}