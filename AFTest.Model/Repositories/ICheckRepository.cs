﻿using System.Collections.Generic;

namespace AFTest.Model {
	public interface ICheckRepository {
		void Add(Check entity);
		void Update(Check entity);

		IEnumerable<Check> GetChecks();
	}
}