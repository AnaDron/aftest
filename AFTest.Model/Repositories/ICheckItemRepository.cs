﻿using System.Collections.Generic;

namespace AFTest.Model {
	public interface ICheckItemRepository {
		IEnumerable<CheckItem> GetCheckItems(int checkId);
	}
}