﻿namespace AFTest.Model {
	public class CheckItem {
		public int CheckItemId { get; set; }
		public double Count { get; set; }
		public double Amount { get; set; }

		public Product Product { get; set; }
		public Check Check { get; set; }
	}
}