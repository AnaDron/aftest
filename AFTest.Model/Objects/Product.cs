﻿namespace AFTest.Model {
	public class Product {
		public int ProductId { get; set; }
		public string Name { get; set; }
		public double Quantity { get; set; }
		public double Price { get; set; }
	}
}