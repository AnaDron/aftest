﻿using System.Collections.Generic;

namespace AFTest.Model {
	public class Check{
		public int CheckId { get; set; }
		public double Sum { get; set; }

		public List<CheckItem> Items { get; set; } = new List<CheckItem>();
	}
}