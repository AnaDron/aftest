﻿namespace AFTest.Model {
	public class CheckService:ICheckService {
		readonly ICheckRepository _checkRepository;

		public CheckService(ICheckRepository checkRepository) {
			_checkRepository = checkRepository;
		}

		public Check Open() {
			var check = new Check();
			_checkRepository.Add(check);
			return check;
		}

		public void Close(Check check) {
			foreach (var item in check.Items) item.Product.Quantity -= item.Count;
			_checkRepository.Update(check);
		}
	}
}