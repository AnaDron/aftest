﻿namespace AFTest.Model {
	public interface ICheckService {
		Check Open();
		void Close(Check check);
	}
}