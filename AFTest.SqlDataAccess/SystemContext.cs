﻿using System.Data.Entity;
using AFTest.Model;

namespace AFTest.SqlDataAccess {
	public class SystemContext:DbContext {
		public SystemContext() : base("Name=AFTest") { }

		public SystemContext(string connString) : base(connString) { }

		public DbSet<Product> Products { get; set; }
		public DbSet<Check> Checks { get; set; }
		public DbSet<CheckItem> CheckItems { get; set; }
	}
}
