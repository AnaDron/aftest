namespace AFTest.SqlDataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CheckItems",
                c => new
                    {
                        CheckItemId = c.Int(nullable: false, identity: true),
                        Count = c.Double(nullable: false),
                        Amount = c.Double(nullable: false),
                        Check_CheckId = c.Int(),
                        Product_ProductId = c.Int(),
                    })
                .PrimaryKey(t => t.CheckItemId)
                .ForeignKey("dbo.Checks", t => t.Check_CheckId)
                .ForeignKey("dbo.Products", t => t.Product_ProductId)
                .Index(t => t.Check_CheckId)
                .Index(t => t.Product_ProductId);
            
            CreateTable(
                "dbo.Checks",
                c => new
                    {
                        CheckId = c.Int(nullable: false, identity: true),
                        Sum = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.CheckId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        Quantity = c.Double(nullable: false),
                        Price = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ProductId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CheckItems", "Product_ProductId", "dbo.Products");
            DropForeignKey("dbo.CheckItems", "Check_CheckId", "dbo.Checks");
            DropIndex("dbo.CheckItems", new[] { "Product_ProductId" });
            DropIndex("dbo.CheckItems", new[] { "Check_CheckId" });
            DropTable("dbo.Products");
            DropTable("dbo.Checks");
            DropTable("dbo.CheckItems");
        }
    }
}
