namespace AFTest.SqlDataAccess.Migrations {
	using System.Data.Entity.Migrations;

	sealed class Configuration:DbMigrationsConfiguration<SystemContext> {
		public Configuration() {
			AutomaticMigrationsEnabled = false;

			SetSqlGenerator("MySql.Data.MySqlClient", new MySql.Data.Entity.MySqlMigrationSqlGenerator());
		}

		protected override void Seed(SystemContext context) {
			//  This method will be called after migrating to the latest version.

			//  You can use the DbSet<T>.AddOrUpdate() helper extension method 
			//  to avoid creating duplicate seed data.
		}
	}
}
