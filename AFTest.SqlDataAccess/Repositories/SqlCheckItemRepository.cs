using System.Collections.Generic;
using System.Linq;
using AFTest.Model;

namespace AFTest.SqlDataAccess {
	public class SqlCheckItemRepository:ICheckItemRepository {
		readonly SystemContext _context;

		public SqlCheckItemRepository(SystemContext context) {
			_context = context;
		}

		public IEnumerable<CheckItem> GetCheckItems(int checkId) {
			var checkItems = (from c in _context.CheckItems where c.Check.CheckId == checkId select c).AsEnumerable();
			return checkItems;
		}
	}
}