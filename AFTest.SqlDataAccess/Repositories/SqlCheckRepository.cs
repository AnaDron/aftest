﻿using System.Collections.Generic;
using System.Linq;
using AFTest.Model;

namespace AFTest.SqlDataAccess {
	public class SqlCheckRepository:ICheckRepository {
		readonly SystemContext _context;

		public SqlCheckRepository(SystemContext context) {
			_context = context;
		}

		public void Add(Check entity) {
			_context.Checks.Add(entity);
			_context.SaveChanges();
		}

		public void Update(Check entity) {
			_context.SaveChanges();
		}

		public IEnumerable<Check> GetChecks() {
			var checks = (from c in _context.Checks select c).AsEnumerable();
			return checks;
		}
	}
}