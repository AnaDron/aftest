﻿using System.Collections.Generic;
using System.Linq;
using AFTest.Model;

namespace AFTest.SqlDataAccess {
	public class SqlProductRepository:IProductRepository {
		readonly SystemContext _context;

		public SqlProductRepository(SystemContext context) {
			_context = context;
		}

		public IEnumerable<Product> GetProductsByNameStartsWith(string nameStart) {
			var products = (from p in _context.Products where p.Quantity > 0 where p.Name.StartsWith(nameStart) select p).AsEnumerable();
			return products;
		}
	}
}