﻿using System.Windows.Controls;
using System.Windows.Interactivity;

namespace AFTest {
	class TextBoxSelectAllOnSourceChangedBehavior:Behavior<TextBox> {
		protected override void OnAttached() {
			base.OnAttached();

			AssociatedObject.Loaded += AssociatedObject_Loaded;
			AssociatedObject.TargetUpdated += AssociatedObject_TargetUpdated;
		}

		protected override void OnDetaching() {
			base.OnDetaching();

			if (AssociatedObject != null) {
				AssociatedObject.Loaded -= AssociatedObject_Loaded;
				AssociatedObject.TargetUpdated -= AssociatedObject_TargetUpdated;
			}
		}

		void AssociatedObject_Loaded(object sender, System.Windows.RoutedEventArgs e) {
			AssociatedObject.SelectAll();
		}

		void AssociatedObject_TargetUpdated(object sender, System.Windows.Data.DataTransferEventArgs e) {
			AssociatedObject.SelectAll();
		}
	}
}