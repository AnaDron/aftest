﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace AFTest {
	class DataGridFixSelectedItemOnLoadBehavior:Behavior<DataGrid> {
		protected override void OnAttached() {
			base.OnAttached();

			AssociatedObject.Loaded += AssociatedObject_Loaded;
		}

		protected override void OnDetaching() {
			base.OnDetaching();

			if (AssociatedObject != null)
				AssociatedObject.Loaded -= AssociatedObject_Loaded;
		}

		void AssociatedObject_Loaded(object sender, RoutedEventArgs e) {
			var selectedItem = AssociatedObject.SelectedItem;
			AssociatedObject.SelectedItem = null;
			AssociatedObject.SelectedItem = selectedItem;
		}
	}
}