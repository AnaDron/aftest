﻿using System.Windows;
using System.Windows.Interactivity;

namespace AFTest {
	class FocusBehavior:Behavior<FrameworkElement> {
		protected override void OnAttached() {
			base.OnAttached();

			AssociatedObject.Loaded += AssociatedObject_Loaded;
		}

		protected override void OnDetaching() {
			base.OnDetaching();

			if (AssociatedObject != null)
				AssociatedObject.Loaded -= AssociatedObject_Loaded;
		}

		void AssociatedObject_Loaded(object sender, RoutedEventArgs e) {
			AssociatedObject.Focus();
		}
	}
}