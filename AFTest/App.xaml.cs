﻿using System.Windows;
using AFTest.Model;
using AFTest.SqlDataAccess;
using AFTest.ViewModel;

namespace AFTest {
	/// <summary>
	/// Логика взаимодействия для App.xaml
	/// </summary>
	public partial class App {
		protected override void OnStartup(StartupEventArgs e) {
			base.OnStartup(e);

			var dbContext = new SystemContext("Name=AFTest");
			dbContext.Database.CreateIfNotExists();
			SeedData.Initialize(dbContext);
			
			var productRepository = new SqlProductRepository(dbContext);
			var checkRepository = new SqlCheckRepository(dbContext);
			var checkItemRepository = new SqlCheckItemRepository(dbContext);

			var checkService = new CheckService(checkRepository);

			var window = new MainWindow();
			window.DataContext = new MainWindowModel(productRepository, checkService);

			window.Show();

			//dbContext.Dispose();
		}
	}
}
