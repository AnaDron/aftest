﻿using System.Linq;
using AFTest.Model;
using AFTest.SqlDataAccess;

namespace AFTest {
	static class SeedData {
		public static void Initialize(SystemContext context) {
			if (context.Products.Any()) return;

			context.Products.AddRange(new[] {
				new Product { Name = "Мороженое детское", Price = 30, Quantity = 100 },
				new Product { Name = "Пельмени", Price = 100, Quantity = 100 },
				new Product { Name = "Печенье", Price = 30, Quantity = 100 },
				new Product { Name = "Пряники", Price = 40, Quantity = 1 },
				new Product { Name = "Конфеты", Price = 200, Quantity = 10 },
				new Product { Name = "Колбаса", Price = 150, Quantity = 5 },
				new Product { Name = "Коньяк", Price = 700, Quantity = 2 },
			});

			context.SaveChanges();
		}
	}
}