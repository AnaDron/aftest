﻿using System.Globalization;
using System.Windows.Markup;

namespace AFTest {
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow {
		public MainWindow() {
			Language = XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag);
			InitializeComponent();
		}
	}
}
